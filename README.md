# recipe-app-api-proxy

# Recipe API proxy

NGINX proxy app for app recipe app API

## Usage 

### Environment Variables

* `LISTEN_PORT` - port to listen to (default  8000)
* `APP_HOST`  - hostname of app to forward requests to (default `app`)
* `APP_PORT` - port of the app to forward requests to (default `9000`)
